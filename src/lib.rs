mod internal;

use std::{collections::VecDeque, io::Cursor, pin::Pin};

use bytes::Bytes;
use fastrand::Rng;
use futures_core::Stream;
use internal::{SendRead, UnsendRead};
use mime::Mime;
use tokio::io::AsyncRead;
use tokio_util::io::ReaderStream;

pub struct Body<R> {
    stream: ReaderStream<internal::Body<R>>,
    boundary: String,
}

pub struct BodyBuilder<R> {
    boundary: String,
    parts: Vec<Part<R>>,
}

pub struct Part<R> {
    name: String,
    content_type: Option<Mime>,
    filename: Option<String>,
    reader: R,
}

fn content_type(boundary: &str) -> mime::Mime {
    format!("multipart/form-data; boundary={}", boundary)
        .parse()
        .expect("Valid mime for content_type")
}

impl<R> Body<R> {
    pub fn content_type(&self) -> mime::Mime {
        content_type(&self.boundary)
    }
}

impl<'a> Body<SendRead<'a>> {
    pub fn builder() -> BodyBuilder<SendRead<'a>> {
        let mut rng = Rng::new();
        let boundary = (0..6).map(|_| rng.alphanumeric()).collect::<String>();

        BodyBuilder {
            boundary,
            parts: Vec::new(),
        }
    }
}

impl<'a> BodyBuilder<SendRead<'a>> {
    pub fn boundary<S: Into<String>>(mut self, boundary: S) -> Self {
        self.boundary = boundary.into();
        self
    }

    pub fn append(mut self, part: Part<SendRead<'a>>) -> Self {
        self.parts.push(part);
        self
    }

    pub fn append_unsend(self, part: Part<UnsendRead<'a>>) -> BodyBuilder<UnsendRead<'a>> {
        let mut parts: Vec<_> = self
            .parts
            .into_iter()
            .map(Part::<UnsendRead<'a>>::from)
            .collect();

        parts.push(part);

        BodyBuilder {
            boundary: self.boundary,
            parts,
        }
    }

    pub fn build(self) -> Body<SendRead<'a>> {
        let parts = self
            .parts
            .into_iter()
            .map(Part::<SendRead<'a>>::build)
            .collect();

        Body {
            stream: ReaderStream::new(internal::Body::new(self.boundary.clone(), parts)),
            boundary: self.boundary,
        }
    }

    pub fn build_part<S: Into<String>>(self, name: S) -> Part<SendRead<'a>> {
        let parts = self
            .parts
            .into_iter()
            .map(Part::<SendRead<'a>>::build)
            .collect();

        Part {
            name: name.into(),
            content_type: Some(content_type(&self.boundary)),
            filename: None,
            reader: SendRead(Box::pin(internal::Body::new(self.boundary.clone(), parts))),
        }
    }
}

impl<'a> BodyBuilder<UnsendRead<'a>> {
    pub fn boundary<S: Into<String>>(mut self, boundary: S) -> Self {
        self.boundary = boundary.into();
        self
    }

    pub fn append(mut self, part: Part<SendRead<'a>>) -> Self {
        self.parts.push(From::from(part));
        self
    }

    pub fn append_unsend(mut self, part: Part<UnsendRead<'a>>) -> BodyBuilder<UnsendRead<'a>> {
        self.parts.push(part);
        self
    }

    pub fn build(self) -> Body<UnsendRead<'a>> {
        let parts: VecDeque<internal::Part<UnsendRead<'a>>> = self
            .parts
            .into_iter()
            .map(Part::<UnsendRead<'a>>::build)
            .collect();

        Body {
            stream: ReaderStream::new(internal::Body::new(self.boundary.clone(), parts)),
            boundary: self.boundary,
        }
    }

    pub fn build_part<S: Into<String>>(self, name: S) -> Part<UnsendRead<'a>> {
        let parts = self
            .parts
            .into_iter()
            .map(Part::<UnsendRead<'a>>::build)
            .collect();

        Part {
            name: name.into(),
            content_type: Some(content_type(&self.boundary)),
            filename: None,
            reader: UnsendRead(Box::pin(internal::Body::new(self.boundary.clone(), parts))),
        }
    }
}

fn encode(value: String) -> String {
    value.replace('"', "\\\"")
}

impl<'a> Part<SendRead<'a>> {
    pub fn new<S: Into<String>, R: AsyncRead + Send + 'a>(name: S, reader: R) -> Self {
        Part {
            name: name.into(),
            content_type: None,
            filename: None,
            reader: SendRead(Box::pin(reader)),
        }
    }

    pub fn new_unsend<S: Into<String>, R: AsyncRead + 'a>(
        name: S,
        reader: R,
    ) -> Part<UnsendRead<'a>> {
        Part {
            name: name.into(),
            content_type: None,
            filename: None,
            reader: UnsendRead(Box::pin(reader)),
        }
    }

    pub fn new_str<S: Into<String>>(name: S, text: &'a str) -> Self {
        Self::new(name, text.as_bytes()).content_type(mime::TEXT_PLAIN)
    }

    pub fn new_string<S1: Into<String>, S2: Into<String>>(name: S1, text: S2) -> Self {
        Self::new(name, Cursor::new(text.into())).content_type(mime::TEXT_PLAIN)
    }

    pub fn content_type(mut self, content_type: mime::Mime) -> Self {
        self.content_type = Some(content_type);
        self
    }

    pub fn filename<S: Into<String>>(mut self, filename: S) -> Self {
        self.filename = Some(filename.into());
        self
    }

    fn build(self) -> internal::Part<SendRead<'a>> {
        let content_type = self.content_type.unwrap_or(mime::APPLICATION_OCTET_STREAM);

        let name = encode(self.name);
        let filename = self.filename.map(encode);

        let content_disposition = if let Some(filename) = filename {
            format!("form-data; name=\"{name}\"; filename=\"{filename}\"")
        } else {
            format!("form-data; name=\"{name}\"")
        };

        internal::Part::<SendRead<'a>>::new(
            content_type.to_string(),
            content_disposition,
            self.reader,
        )
    }
}
impl<'a> Part<UnsendRead<'a>> {
    pub fn content_type(mut self, content_type: mime::Mime) -> Self {
        self.content_type = Some(content_type);
        self
    }

    pub fn filename<S: Into<String>>(mut self, filename: S) -> Self {
        self.filename = Some(filename.into());
        self
    }

    fn build(self) -> internal::Part<UnsendRead<'a>> {
        let content_type = self.content_type.unwrap_or(mime::APPLICATION_OCTET_STREAM);

        let name = encode(self.name);
        let filename = self.filename.map(encode);

        let content_disposition = if let Some(filename) = filename {
            format!("form-data; name=\"{name}\"; filename=\"{filename}\"")
        } else {
            format!("form-data; name=\"{name}\"")
        };

        internal::Part::<UnsendRead<'a>>::new(
            content_type.to_string(),
            content_disposition,
            self.reader,
        )
    }
}

impl<R> Stream for Body<R>
where
    R: AsyncRead + Unpin,
{
    type Item = std::io::Result<Bytes>;

    fn poll_next(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        Pin::new(&mut self.get_mut().stream).poll_next(cx)
    }
}

impl<'a> From<Part<SendRead<'a>>> for Part<UnsendRead<'a>> {
    fn from(value: Part<SendRead<'a>>) -> Self {
        Self {
            name: value.name,
            content_type: value.content_type,
            filename: value.filename,
            reader: UnsendRead::from(value.reader),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{future::poll_fn, io::Cursor, pin::Pin};

    struct Streamer<S>(S);

    impl<S> Streamer<S> {
        async fn next(&mut self) -> Option<S::Item>
        where
            S: futures_core::Stream + Unpin,
        {
            poll_fn(|cx| Pin::new(&mut self.0).poll_next(cx)).await
        }
    }

    fn assert_stream<S: futures_core::Stream>() {}

    #[test]
    fn impl_asserts() {
        assert_stream::<super::Body<super::SendRead<'static>>>();
        assert_stream::<super::Body<super::UnsendRead<'static>>>();
    }

    #[tokio::test]
    async fn build_text() {
        let body = super::Body::builder()
            .boundary(String::from("hello"))
            .append(super::Part::new_str(String::from("first_name"), "John"))
            .append(super::Part::new_str(String::from("last_name"), "Doe"))
            .build();

        let mut out = Vec::new();

        let mut streamer = Streamer(body);

        while let Some(res) = streamer.next().await {
            out.extend(res.expect("read success"));
        }

        let out = String::from_utf8(out).expect("Valid string");

        assert_eq!(out, "--hello\r\ncontent-type: text/plain\r\ncontent-disposition: form-data; name=\"first_name\"\r\n\r\nJohn\r\n--hello\r\ncontent-type: text/plain\r\ncontent-disposition: form-data; name=\"last_name\"\r\n\r\nDoe\r\n--hello--\r\n")
    }

    #[tokio::test]
    async fn test_form_body_stream() {
        let path = "./files/Shenzi.webp";
        let file = tokio::fs::File::open(path).await.expect("Opened file");

        let body = super::Body::builder()
            .boundary(String::from("hello"))
            .append(super::Part::new_str("name1", "value1"))
            .append(super::Part::new_str("name2", "value2"))
            .append(
                super::Part::new("images[]", file)
                    .filename("Shenzi.webp")
                    .content_type("image/webp".parse().expect("Parsed mime")),
            )
            .append(super::Part::new("input", Cursor::new("Hello World!")))
            .build();

        let mut out = Vec::new();

        let mut streamer = Streamer(body);

        while let Some(res) = streamer.next().await {
            out.extend(res.expect("read success"));
        }

        let file_contents = tokio::fs::read(path).await.expect("Read file");

        let expected = [
            &b"--hello\r\n"[..],
            &b"content-type: text/plain\r\n"[..],
            &b"content-disposition: form-data; name=\"name1\"\r\n"[..],
            &b"\r\n"[..],
            &b"value1\r\n"[..],
            &b"--hello\r\n"[..],
            &b"content-type: text/plain\r\n"[..],
            &b"content-disposition: form-data; name=\"name2\"\r\n"[..],
            &b"\r\n"[..],
            &b"value2\r\n"[..],
            &b"--hello\r\n"[..],
            &b"content-type: image/webp\r\n"[..],
            &b"content-disposition: form-data; name=\"images[]\"; filename=\"Shenzi.webp\"\r\n"[..],
            &b"\r\n"[..],
            &file_contents[..],
            &b"\r\n"[..],
            &b"--hello\r\n"[..],
            &b"content-type: application/octet-stream\r\n"[..],
            &b"content-disposition: form-data; name=\"input\"\r\n"[..],
            &b"\r\n"[..],
            &b"Hello World!\r\n"[..],
            &b"--hello--\r\n"[..],
        ]
        .into_iter()
        .map(|s| s.iter().copied())
        .flatten()
        .collect::<Vec<u8>>();

        if out != expected {
            tokio::fs::write("./simple.actual.multipart", out)
                .await
                .expect("Wrote file");
            tokio::fs::write("./simple.expected.multipart", expected)
                .await
                .expect("Wrote file");
            panic!("No match")
        }
    }

    struct MyBoundary;
    impl common_multipart_rfc7578::client::multipart::BoundaryGenerator for MyBoundary {
        fn generate_boundary() -> String {
            String::from("MYBOUNDARY")
        }
    }

    #[tokio::test]
    async fn compare_feristseng() {
        let mut form = common_multipart_rfc7578::client::multipart::Form::new::<MyBoundary>();
        let path1 = "./files/Shenzi.webp";
        let path2 = "./files/Shenzi.png";
        form.add_text("meowdy", "y'all");
        form.add_reader_file_with_mime(
            "images[]",
            std::fs::File::open(path1).expect("Opened file"),
            "Shenzi.webp",
            "image/webp".parse().expect("Valid mime"),
        );
        form.add_reader("images[]", std::fs::File::open(path2).expect("Opened file"));
        form.add_text("howdy", "y'all");

        let body = super::Body::builder()
            .boundary("MYBOUNDARY")
            .append(super::Part::new_str("meowdy", "y'all"))
            .append(
                super::Part::new(
                    "images[]",
                    tokio::fs::File::open(path1).await.expect("Opened file"),
                )
                .filename("Shenzi.webp")
                .content_type("image/webp".parse().expect("Valid mime")),
            )
            .append(super::Part::new(
                "images[]",
                tokio::fs::File::open(path2).await.expect("Opened file"),
            ))
            .append(super::Part::new_str("howdy", "y'all"))
            .build();

        let mut ferrisbody: Streamer<common_multipart_rfc7578::client::multipart::Body> =
            Streamer(form.into());
        let mut mybody = Streamer(body);

        let mut ferriststeng = Vec::new();
        let mut mine = Vec::new();

        while let Some(res) = ferrisbody.next().await {
            ferriststeng.extend(res.expect("read success"));
        }
        while let Some(res) = mybody.next().await {
            mine.extend(res.expect("read success"));
        }

        if mine != ferriststeng {
            tokio::fs::write("./compare.actual.multipart", mine)
                .await
                .expect("Wrote file");
            tokio::fs::write("./compare.expected.multipart", ferriststeng)
                .await
                .expect("Wrote file");
            panic!("No match")
        }
    }

    #[test]
    fn encode() {
        let cases = [("hello", "hello"), ("Hello \"John\"", "Hello \\\"John\\\"")];

        for (input, expected) in cases {
            let output = super::encode(String::from(input));

            assert_eq!(output, expected);
        }
    }
}
